import './App.css';
import MyShop from './component/MyShop';

function App() {
  return (
    <div className="App">

      <MyShop/>

    </div>
  );
}

export default App;
