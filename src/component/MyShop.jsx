import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Table,Button, CardGroup,Card  } from 'react-bootstrap';
export default class MyShop extends Component {
  constructor(){
    super();
    this.state={
      data:[
        {
          id: 1,
          name: "Coca",
          isSelect: false
        },
        {
          id: 2,
          name: "Pepsi",
          isSelect: true
        },
        {
          id: 3,
          name: "Sting",
          isSelect: false
        },
        {
          id: 4,
          name: "Milk",
          isSelect: false
        }
      ]
    };

  }

  onSelect(index){

    let uData = this.state.data
    uData[index].isSelect = !uData[index].isSelect

    this.setState({
      data: uData
    })

  }
  //  onSelectOr(indexOrder){
    //  let OrData=this.state.data
    //  OrData[indexOrder].isSelect=!OrData[indexOrder].isSelect
    //  this.setState( {
        // data:OrData
      // } )
  //  }


  render() {
    return (
      <div>

          {/* head  */}
          <h3 style={{color:'blue'}}>Wellcome to My Shop</h3>

          {/* tableFirst */}
  <Container>
     <Table striped bordered hover style={{border:'2px solid green', backgroundColor:"silver",}}>
     <thead>
    <tr>
      <th>#</th>
      <th>Product Name</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    {
      this.state.data.map((product,index)=>
        <tr>
        <td>{product.id}</td>
        <td>{product.name}</td>
        <td><Button onClick={()=>{this.onSelect(index)}} variant={product.isSelect === true ? "danger":"success"}>{product.isSelect === true ? "unselect":"select"}</Button>{' '}</td>
      </tr>
      )
    }
   
  </tbody>
   </Table>

         {/* second */}
 <CardGroup style={{border:'2px solid green', backgroundColor:'teal'}} >
   {
     this.state.data.map((order,index)=>
     <Card style={{margin:'30px', border:'2px solid red'}} >
     <Card.Img style={{height:'200px', widht:'70px'}} variant="top" src= "https://www.packagingdigest.com/sites/packagingdigest.com/files/AdobeStock_279692163_Editorial_Use_Only-Beverage-FTR-new.jpg"/>
    <Card.Body>
      <Card.Title>{order.id}</Card.Title>
      <Card.Text>
        {order.name}
      </Card.Text>
      <Card.Text>
      <Button onClick={()=>{this.onSelect(index)}} variant={ order.isSelect===true ?"danger":"success" }>{order.isSelect===true? "unselect":"select"}</Button>{' '}
      </Card.Text>
    </Card.Body>
  </Card>
  
     )

  }
</CardGroup>
       </Container>

      </div>
    )
  }
}
